import { Controller, Get } from '@nestjs/common';
import { DeviceService } from './device.service';
import {MessagePattern, Payload} from "@nestjs/microservices";

@Controller()
export class DeviceController {
  constructor(private readonly deviceService: DeviceService) {}

  @Get()
  getHello(): string {
    return this.deviceService.getHello();
  }

  @MessagePattern("find:user:active:device")
  async findActiveUserDevice(@Payload() data: any) {
    return this.deviceService.findUserActiveDevice(data)
  }

  @MessagePattern("create:user:active:device")
  async createUserActiveDevice(@Payload() data: any) {
    return this.deviceService.createUserActiveDevice(data)
  }

  @MessagePattern("update:user:active:device")
  async updateUserActiveDevice(@Payload() data: any) {
    return this.deviceService.updateUserActiveDevice(data);
  }

  @MessagePattern("generate:verification:code")
  async generateVerificationCode(@Payload() device: any) {
    return this.deviceService.generateVerificationCode(device)
  }

  @MessagePattern("verify:device:activation:code")
  async verifyDeviceActivationCode(@Payload() deviceToVerify: any) {
    return this.deviceService.verifyDeviceActivationCode(deviceToVerify)
  }

  @MessagePattern("set:device:value")
  async setDeviceValues(@Payload() deviceInfo: any) {
    console.log("deviceInfo: ", deviceInfo)
    return this.deviceService.setDeviceValues(deviceInfo.criteria, deviceInfo.partialValues)
  }
}
