import { NestFactory } from '@nestjs/core';
import { DeviceModule } from './device.module';
import {MicroserviceOptions, Transport} from "@nestjs/microservices";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(DeviceModule, {
    transport: Transport.TCP,
    options: {
      host: 'connect_device',
      port: 4000,
    }
  });
  await app.listen();
}
bootstrap();
