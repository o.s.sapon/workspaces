import { Module } from '@nestjs/common';
import { DeviceController } from './device.controller';
import { DeviceService } from './device.service';
import {DatabaseModule} from "@app/database";
import {TypeOrmModule} from "@nestjs/typeorm";
import {DeviceEntity} from "@app/database/entities/device/device.entity";

@Module({
  imports: [
    DatabaseModule,
    TypeOrmModule.forFeature([DeviceEntity])
  ],
  controllers: [DeviceController],
  providers: [DeviceService],
})
export class DeviceModule {}
