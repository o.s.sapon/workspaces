import {BadRequestException, Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {DeviceEntity} from "@app/database/entities/device/device.entity";
import {Repository} from "typeorm";

@Injectable()
export class DeviceService {
  constructor(
    @InjectRepository(DeviceEntity)
    private readonly deviceRepository: Repository<DeviceEntity>
  ) {
  }

  getHello(): string {
    return "Hello World!";
  }

  async findOne(criteria) {
    return this.deviceRepository.findOne({
      where: {
        ...criteria
      }
    });
  }

  async update(criteria, partialEntity) {
    return this.deviceRepository.update(criteria, partialEntity);
  }

  async findUserActiveDevice(data) {
    return this.findOne({
      userId: data.userId,
      app: data.app,
      active: true
    });
  }

  async createUserActiveDevice(data) {
    const activeDevice = this.deviceRepository.create({
      active: true,
      app: data.app,
      userId: data.userId,
      name: data.name,
      identifier: data.identifier
    });
    return this.deviceRepository.save(activeDevice);
  }

  async setDeviceInActive(device, active = false) {
    return this.update({
      userId: device.userId,
      active: !active,
      name: device.name,
      identifier: device.identifier,
      app: device.app
    }, {
      active
    });
  }

  async updateUserActiveDevice(data) {
    await this.setDeviceInActive(data.active)
    const activeDevice = await this.findOne(data.loggedWith)
    if (!activeDevice) {
      return this.createUserActiveDevice(data.loggedWith)
    }
    await this.setDeviceInActive(data.loggedWith, true)
    return this.findUserActiveDevice(data.loggedWith)
  }

  async generateVerificationCode(deviceToVerify) {
    const activationCode = this.generateActivationCode(4);
    const deviceWithCode = await this.update({
      userId: deviceToVerify.userId,
      active: true,
      name: deviceToVerify.name,
      identifier: deviceToVerify.identifier,
      app: deviceToVerify.app
    }, {
      activationCode
    })
    if (deviceWithCode.affected) {
      deviceToVerify.activationCode = activationCode
    }
    return deviceToVerify
  }

  generateActivationCode(length: number): string {
    let code = "";
    for (let i = 0; i < length; i++) {
      code += Math.floor(Math.random() * 9);
    }
    return code;
  }

  async verifyDeviceActivationCode(deviceToVerify) {
    const deviceVerified = await this.findOne(deviceToVerify)
    if (deviceVerified) {
      await this.update({id: deviceVerified.id}, {
        activationCode: null
      })
      deviceVerified.activationCode = null
    }
    return deviceVerified
  }

  async setDeviceValues(criteria, partialValues) {
    const deviceToUpdate = await this.update(criteria, partialValues);
    if (!deviceToUpdate.affected) {
      throw new BadRequestException("Not Updated");
    }
    return this.findOne({
      userId: criteria.userId,
      app: criteria.app,
      active: true,
    })
  }
}
