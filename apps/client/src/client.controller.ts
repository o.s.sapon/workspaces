import {Controller, Get, NotFoundException} from "@nestjs/common";
import { ClientService } from './client.service';
import {MessagePattern, Payload, RpcException} from "@nestjs/microservices";

export type Login = string

@Controller()
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Get()
  getHello(): string {
    return this.clientService.getHello();
  }

  @MessagePattern("check:client:exists:create:or:restore")
  async findClientOrCreate(@Payload('login') login: Login) {
    let client = await this.clientService.findWithSoftDeletedByLogin(login)
    if (!client) {
      client = this.clientService.createEntityWithLogin(login)
      await this.clientService.save(client)
      return {
        uuid: client.uuid,
        login: client.login
      }
    }
    if (this.clientService.isClientSoftDeleted(client)) {
      await this.clientService.restoreClient(client)
      client.deletedAt = null
    }
    console.log("client: ", client)
    return {
      uuid: client.uuid,
      login: client.login
    }
  }

  @MessagePattern("get:client:uuid:identifier:by:login")
  async getClientUuidByLogin(@Payload('login') login: Login) {
    let client = await this.clientService.findOne({
      select: ['login', 'uuid'],
      where: {
        login
      },
    })
    if (!client) {
      return new NotFoundException();
    }
    return {
      uuid: client.uuid,
      login: client.login
    }
  }
}