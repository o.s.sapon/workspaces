import {NestFactory} from "@nestjs/core";
import {ClientModule} from "./client.module";
import {MicroserviceOptions, Transport} from "@nestjs/microservices";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(ClientModule, {
    transport: Transport.TCP,
    options: {
      host: 'connect_client',
      port: 4000,
    }
  });
  await app.listen();
}
bootstrap();
