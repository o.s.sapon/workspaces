import { Module } from '@nestjs/common';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ClientEntity, DatabaseModule} from "@app/database";

@Module({
  imports: [
    DatabaseModule,
    TypeOrmModule.forFeature([ClientEntity])
  ],
  controllers: [ClientController],
  providers: [ClientService],
})
export class ClientModule {}
