import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {ClientEntity, UserEntity} from "@app/database";
import {Repository} from "typeorm";

@Injectable()
export class ClientService {
  constructor(
    @InjectRepository(ClientEntity)
    private readonly clientRepository: Repository<ClientEntity>
  ) {
  }

  getHello(): string {
    return 'Hello World!';
  }

  async findWithSoftDeletedByLogin(login: string) {
    return await this.findOne({
      where: {
        login
      },
      withDeleted: true
    })
  }

  createEntityWithLogin(login: string) {
    return this.clientRepository.create({login})
  }

  isClientSoftDeleted(client): boolean {
    return client.deletedAt !== null
  }

  async restoreClient(client) {
    return this.clientRepository.restore(client.id)
  }

  async save(client) {
    return this.clientRepository.save(client)
  }

  async findOne(criteria) {
    return this.clientRepository.findOne({
      ...criteria
    })
  }
}
