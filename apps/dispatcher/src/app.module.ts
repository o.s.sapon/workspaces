import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { BalanceModule } from './balance/balance.module';
import {DatabaseModule} from "@app/database";
import {ClientBalance, ClientClass} from "./classes/users/base/user.class";
import {ClientsModule, Transport} from "@nestjs/microservices";
import {CLIENT_SERVICE, DEVICE_SERVICE} from "./config/constants";

@Module({
  imports: [
    ClientsModule.register([{
      name: CLIENT_SERVICE,
      transport: Transport.TCP,
      options: {
        host: 'connect_client',
        port: 4000
      }},
    ]),
    ClientsModule.register([{
      name: DEVICE_SERVICE,
      transport: Transport.TCP,
      options: {
        host: 'connect_device',
        port: 4000
      }},
    ]),
    DatabaseModule,
    AuthModule,
    BalanceModule
  ],
  controllers: [],
  providers: [
    ClientClass,
    ClientBalance
  ],
})
export class AppModule {}
