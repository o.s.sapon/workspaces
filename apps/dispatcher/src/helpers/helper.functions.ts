import {ArgumentMetadata} from "@nestjs/common";
import {validate, ValidationError} from "class-validator";
import {ClassConstructor, plainToInstance} from "class-transformer";

export async function getValidationPipeErrors(metatype: Function | ArgumentMetadata, value: any) : Promise<ValidationError[] | any> {
  if (!metatype || !toValidateMetatype(metatype as Function)) {
    return value;
  }
  const object = plainToInstance(metatype as ClassConstructor<any>, value);
  return await validate(object);
}

export function toValidateMetatype(metatype: Function) {
  const types: Function[] = [String, Boolean, Number, Array, Object];
  return !types.includes(metatype);
}