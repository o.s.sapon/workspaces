import {Inject, Injectable} from "@nestjs/common";
import {ClientProxy} from "@nestjs/microservices";

export interface IDevice {
  identifier: string
  name: string
  app: string
  pushToken: string
  locale: string
}

export interface IProfile {
  firstname: string
  lastname: string
  patronymic: string
  badge: string
  birthday: Date
}

export interface IUser {
  id: number
  uuid: string
  login: string
  password: string | null
  blockedTimes: number
  blockedTill: Date | null
  languages: string[]
  devices: IDevice[] | null
  device: IDevice;
  profile: IProfile
}

export class GenerateCodeDto {
  login: string
}

export class CodeDto {

}

export interface AuthableContract {
  auth()
}

export type CodeType = CodeDto | void

export interface VerifiableContract {
  verifyCode(generateCode: GenerateCodeDto) : CodeType
}

export interface ActivitiesContract {
  handleActivity();
}

export interface IBalance {

}

export interface BalanceContract {
  getUserBalance() : IBalance[]
  addToUserBalance(newBalanceRecord: any): IBalance
}

export interface CallableContract {
  call();
}

export abstract class ABalanceClass implements BalanceContract {
  abstract addToUserBalance(newBalanceRecord: any): IBalance ;

  abstract getUserBalance(): IBalance[] ;

}

@Injectable()
export class ClientBalance extends ABalanceClass {
  constructor(
    @Inject('CLIENT_SERVICE')
    private readonly clientClient: ClientProxy
  ) {
    super();
  }

  addToUserBalance(newBalanceRecord: any): IBalance {
    return undefined;
  }

  getUserBalance(): IBalance[] {
    return [];
  }
}

export abstract class AUserClass implements IUser, AuthableContract, ActivitiesContract, CallableContract {
  id: number;
  uuid: string;
  login: string;
  password: string;
  blockedTill: Date;
  blockedTimes: number;
  languages: string[];
  devices: IDevice[];
  device: IDevice;
  profile: IProfile;


  abstract auth();
  abstract call();
  abstract handleActivity();
}

export abstract class AClientClass extends AUserClass implements VerifiableContract, AUserClass {
  abstract verifyCode(generateCode: GenerateCodeDto): CodeType;

}

export class ClientClass implements AClientClass {
  id: number;
  uuid: string;
  login: string;
  password: string;
  blockedTill: Date;
  blockedTimes: number;
  languages: string[];
  devices: IDevice[];
  device: IDevice;
  profile: IProfile;

  constructor(
    values: any
  ) {
    Object.assign(this, values)
  }

  auth() {
  }

  call() {
  }

  handleActivity() {
  }

  verifyCode(generateCode: GenerateCodeDto): CodeType {
    return undefined;
  }

  attachActiveDevice(newActiveDevice: IDevice) {
    this.device = newActiveDevice
  }

}

export abstract class ADevice implements IDevice {
  app: string;
  identifier: string;
  locale: string;
  name: string;
  pushToken: string;
}

export class ClientDevice implements ADevice {
  app: string;
  identifier: string;
  locale: string;
  name: string;
  pushToken: string;
  userId: number;

  constructor(values) {
    Object.assign(this, values)
  }
}

export class AuthClientClassDto {
  client: ClientClass
}
