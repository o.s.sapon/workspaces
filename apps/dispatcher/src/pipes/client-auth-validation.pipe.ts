import {ArgumentMetadata, Inject, Injectable, PipeTransform} from "@nestjs/common";
import {ClientProxy} from "@nestjs/microservices";
import {ClientClass, ClientDevice} from "../classes/users/base/user.class";
import {lastValueFrom} from "rxjs";
import {VerificationCodeIntentDto} from "@app/dto";

@Injectable()
export class ClientDeviceAuthValidationPipe implements PipeTransform {
  constructor(
    @Inject('CLIENT_SERVICE')
    private readonly clientServiceProxy: ClientProxy,
    @Inject('DEVICE_SERVICE')
    private readonly deviceServiceProxy: ClientProxy,
  ) {
  }

  async transform(value: VerificationCodeIntentDto, {metatype}: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const client: ClientClass = new ClientClass(await lastValueFrom(
      this.clientServiceProxy.send("check:client:exists:create:or:restore", {
        login: value.login
      }))
    )
    const loggingWithDevice = new ClientDevice(value.device)
    let activeClientDevice: ClientDevice = await lastValueFrom(
      this.deviceServiceProxy.send('find:user:active:device', {
        userId: client.uuid,
        app: value.device.app
      })
    )
    if (!activeClientDevice) {
      activeClientDevice = await lastValueFrom(
        this.deviceServiceProxy.send('create:user:active:device', {
          userId: client.uuid,
          ...value.device
        })
      )
    }
    if (
      activeClientDevice.identifier != value.device.identifier ||
      activeClientDevice.name !== value.device.name
    ) {
      activeClientDevice = await lastValueFrom(
        this.deviceServiceProxy.send('update:user:active:device', {
          active: activeClientDevice,
          loggedWith: {
            userId: client.uuid,
            ...loggingWithDevice
          }
        })
      )
    }

    client.attachActiveDevice(activeClientDevice)
    return client;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}