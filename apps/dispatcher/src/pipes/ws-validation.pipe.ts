import {ArgumentMetadata, Injectable, ValidationPipe} from "@nestjs/common";
import {WsException} from "@nestjs/websockets";
import {ValidationError} from "class-validator";
import {getValidationPipeErrors} from "../helpers/helper.functions";

@Injectable()
export class WsValidationPipe extends ValidationPipe {
  async transform(value: any, {metatype}: ArgumentMetadata) {

    const errors = await getValidationPipeErrors(metatype, value)

    if (errors.length > 0) {
      throw new WsException({
        statusCode: 400,
        status: "Bad request",
        message: errors.map((error: ValidationError) => ({
          [error.property]: Object.keys(error.constraints).map(key => error.constraints[key])
        }))
      })
    }
    return value;
  }
}