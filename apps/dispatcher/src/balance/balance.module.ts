import { Module } from '@nestjs/common';
import { BalanceController } from './balance.controller';
import {ClientsModule, Transport} from "@nestjs/microservices";

@Module({
  imports: [
    ClientsModule.register([{
      name: 'BALANCE_SERVICE',
      transport: Transport.TCP,
      options: {
        host: 'balance',
        port: 4000
      }},
    ]),
  ],
  controllers: [BalanceController]
})
export class BalanceModule {}
