import {Controller, Get, Inject, UseInterceptors} from "@nestjs/common";
import {ClientProxy} from "@nestjs/microservices";
import {HttpErrorsInterceptor} from "../intercepters/http-error.interceptor";

@Controller('balance')
export class BalanceController {

  constructor(
    @Inject('BALANCE_SERVICE') private client: ClientProxy,
  ) {}

  @UseInterceptors(HttpErrorsInterceptor)
  @Get()
  async test() {
    return this.client.send({ cmd: "balance" }, {})
  }
}
