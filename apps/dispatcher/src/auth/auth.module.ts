import {Module} from "@nestjs/common";
import {ClientsModule, Transport} from "@nestjs/microservices";
import {AuthController} from "./auth.controller";
import {AUTH_SERVICE, BALANCE_SERVICE, CLIENT_SERVICE, DEVICE_SERVICE} from "../config/constants";

@Module({
  imports: [
    ClientsModule.register([{
      name: AUTH_SERVICE,
      transport: Transport.TCP,
      options: {
        host: "connect_auth",
        port: 4000
      }
    }
    ]),
    ClientsModule.register([{
      name: CLIENT_SERVICE,
      transport: Transport.TCP,
      options: {
        host: "connect_client",
        port: 4000
      }
    }
    ]),
    ClientsModule.register([{
      name: BALANCE_SERVICE,
      transport: Transport.TCP,
      options: {
        host: "connect_balance",
        port: 4000
      }
    }
    ]),
    ClientsModule.register([{
      name: DEVICE_SERVICE,
      transport: Transport.TCP,
      options: {
        host: "connect_device",
        port: 4000
      }
    }
    ])
  ],
  controllers: [AuthController],
  providers: []
})
export class AuthModule {
}
