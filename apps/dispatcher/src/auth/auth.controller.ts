import {Body, Controller, HttpCode, Inject, Post} from "@nestjs/common";
import {ClientProxy} from "@nestjs/microservices";
import {DeviceInfoDto, VerificationCodeIntentDto} from "@app/dto";
import {lastValueFrom} from "rxjs";
import {IsDefined, IsNotEmpty, IsNotEmptyObject, IsString, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
import {AUTH_SERVICE, BALANCE_SERVICE, CLIENT_SERVICE} from "../config/constants";


export class ClientAuthDto {
  @IsString()
  @IsNotEmpty()
  login: string

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => DeviceInfoDto)
  readonly device: DeviceInfoDto
}

@Controller('auth')
export class AuthController {

  /*
  *   CLIENT AUTH FLOW
  *     I) RECEIVING OF VERIFICATION CODE
  *       1. receive login and device info
  *       2. check if client exists
  *         2.1 if soft deleted
  *           2.1.1 restore client account
  *         2.2 if not
  *           2.2.1 create client
  *       3. save device as main client device
  *         3.1 get active device by user.id, app type and device.active === true
  *         3.2 if active device identifier === device identifier
  *           3.2.1 generate activation code
  *           3.2.2 update active device activation code property
  *           3.2.3 return active device
  *         3.3 if active device identifier !== device identifier
  *           3.3.1 update device to active === false
  *           3.3.2 get device by user.id and device identifier
  *           3.3.3 create if not exists with active === true
  *             3.3.3.1 generate activation code
  *             3.3.3.2 update active device activation code property
  *             3.3.3.3 return active device
  *       4. send verification code
  *
  *     II) AUTHORIZING IN APP (RECEIVING JWT TOKEN)
  *       1. receive login, verification code and device info with pushToken
  *       2. get client by login with state
  *       3. get active client device by device info
  *         3.1 save push token with active device
  *         3.2 verify verification code vs device verification code
  *           3.2.1 if not verified
  *             3.2.1.1 throw bad request exception
  *           3.2.2 update active device verification code to null
  *       4. check if already authorized in app (token saved in redis by app type and login)
  *         4.1 if already authorized
  *           4.1.1 remove user refresh token
  *         4.2 generate jwt token with user state as body
  *         4.3 generate jwt refresh token with user id to generate new token when refreshing user token
  *         4.4 save refresh token in redis cache by app type and login
  */

  constructor(
    @Inject(AUTH_SERVICE)
    private readonly authServiceClient: ClientProxy,
    @Inject(CLIENT_SERVICE)
    private readonly clientServiceClient: ClientProxy,
    @Inject(BALANCE_SERVICE)
    private readonly balanceServiceClient: ClientProxy,
  ) {
  }

  @Post('/client/get-code')
  @HttpCode(200)
  async getVerificationCode(
    @Body() clientIntentToAuth: VerificationCodeIntentDto
  ) {
    console.log("getVerificationCode: ", clientIntentToAuth)
    const clientUnderVerification = await lastValueFrom(this.authServiceClient.send(
      "generate:verification:code",
      clientIntentToAuth
    ))
    console.log("clientUnderVerification: ", clientUnderVerification)
    return clientUnderVerification
  }

  @Post('/client')
  async handleIntentToAuthorize(@Body() clientAuthDto: ClientAuthDto) {
    console.log("handleIntentToAuthorize: ", clientAuthDto)
    return this.authServiceClient.send("auth:client:with:code", clientAuthDto)
    return clientAuthDto
    // return this.clientServiceClient.send({
    //   cmd: 'create:client:if:not:exists'
    // }, clientAuthDto)
  }



  /*
  *   INTERPRETER AUTH FLOW
  *     I) AUTHORIZING IN APP (RECEIVING JWT TOKEN)
  *       1. sends login, password, device info with pushToken
  *       2. get user by login with state
  *         2.1 if not exists
  *           2.1.1 throw bad request exception
  *       3. verify passwords
  *         3.1 if not verified
  *           3.1.1 throw bad request exception
  *       4. check if already authorized in app (token saved in redis by app type)
  *         4.1 if already authorized
  *           4.1.1 remove user refresh token
  *         4.2 generate jwt token with user state as body
  *         4.3 generate jwt refresh token with user id to generate new token when refreshing user token
  *         4.4 save refresh token in redis cache by app type
  */

  /*
  *   LAPTOP AUTH FLOW
  */

  /*
  *   WIDGET AUTH FLOW
  */
}
