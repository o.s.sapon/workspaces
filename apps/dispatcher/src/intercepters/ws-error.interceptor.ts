import {CallHandler, ExecutionContext, Injectable, NestInterceptor} from "@nestjs/common";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {WsException} from "@nestjs/websockets";

@Injectable()
export class WsErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        catchError(err =>
          throwError(() => {
            console.log("WsErrorsInterceptor: ", {...err});
            return new WsException({...err});
          })
        )
      );
  }
}