import {CallHandler, ExecutionContext, HttpException, Injectable, NestInterceptor} from "@nestjs/common";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable()
export class HttpErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        catchError(err =>
          throwError(() => {
            console.log("ErrorsInterceptor: ",err)
            return new HttpException({...err}, err.statusCode || 500);
          })
        )
      );
  }
}