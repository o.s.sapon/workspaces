import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from "../src/app.module";
import * as io from 'socket.io-client';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.startAllMicroservices()
    await app.init();
  });

  it('/auth (GET)', () => {
    return request(app.getHttpServer())
      .get('/auth')
      .expect(200)
      .expect('Hello World!');
  });
});

describe('WebsocketAdapter', () => {
  let ws: any,
    app: INestApplication,
    connectToSocketIO: () => any;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    ws = io.connect('http://dispatcher:3000/auth')
    await app.init();
    await app.listen(3000)
  })
  afterAll(async () => {
    await ws.disconnect()
    await app.close()
  })

  it('message sends', async () => {
    return ws.on('connect', () => {
      ws.emit('message', {login: '380632613668'}, (payload) => {
        expect(payload).toBe({login: '380632613668'})
      })
    })
  })


})
