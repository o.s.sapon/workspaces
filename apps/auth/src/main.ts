import {NestFactory} from "@nestjs/core";
import {AuthModule} from "./auth.module";
import {MicroserviceOptions, Transport} from "@nestjs/microservices";
import {ValidationPipe} from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AuthModule, {
    transport: Transport.TCP,
    options: {
      host: 'connect_auth',
      port: 4000
    }
  });
  app.useGlobalPipes(new ValidationPipe({
    transform: true
  }))
  await app.listen()
}
bootstrap();
