import {Inject, Injectable} from "@nestjs/common";
import {ClientClass} from "../../dispatcher/src/classes/users/base/user.class";
import {lastValueFrom} from "rxjs";
import {ClientProxy} from "@nestjs/microservices";
import {ConfigService} from "@nestjs/config";

@Injectable()
export class AuthService {

  constructor(
    @Inject("CLIENT_SERVICE")
    private readonly clientServiceClient: ClientProxy,
    @Inject("DEVICE_SERVICE")
    private readonly deviceServiceClient: ClientProxy,
    private readonly configService: ConfigService
  ) {
  }

  getHello(): string {
    return 'Hello World!';
  }

  async generateVerificationCode(clientUnderVerification: ClientClass) {
    const device = await lastValueFrom(this.deviceServiceClient.send(
      'generate:verification:code',
      clientUnderVerification.device
    ))

    if (this.configService.get<boolean>("DEBUG")) {
      return { success: true, activationCode: device.activationCode };
    }
    // TODO notify client with sms service
    // this.notificationsServiceClient.emit('notify:via:sms', {
    //   phone: clientUnderVerification.login,
    //   text: device.activationCode
    // });
    return { success: true };
  }

  async generateJwtToken(verifiedClient) {
    return verifiedClient
  }
}
