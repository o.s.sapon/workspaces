import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import {ClientsModule, Transport} from "@nestjs/microservices";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    ClientsModule.register([{
      name: 'CLIENT_SERVICE',
      transport: Transport.TCP,
      options: {
        host: 'connect_client',
        port: 4000
      }},
    ]),
    ClientsModule.register([{
      name: 'DEVICE_SERVICE',
      transport: Transport.TCP,
      options: {
        host: 'connect_device',
        port: 4000
      }},
    ]),
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
