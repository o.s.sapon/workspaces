import {
  ArgumentMetadata, BadRequestException,
  CallHandler,
  Controller,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
  PipeTransform,
  UseInterceptors,
  UsePipes
} from "@nestjs/common";
import {AuthService} from "./auth.service";
import {ClientProxy, MessagePattern, Payload, RpcException} from "@nestjs/microservices";
import {lastValueFrom, Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {ClientDeviceAuthValidationPipe} from "../../dispatcher/src/pipes/client-auth-validation.pipe";
import {ClientClass, ClientDevice} from "../../dispatcher/src/classes/users/base/user.class";
import {ClientAuthDto} from "../../dispatcher/src/auth/auth.controller";
import {CLIENT_SERVICE, DEVICE_SERVICE} from "../../dispatcher/src/config/constants";


@Injectable()
export class ClientDeviceVerificationValidationPipe implements PipeTransform {
  constructor(
    @Inject(CLIENT_SERVICE)
    private readonly clientServiceClient: ClientProxy,
    @Inject(DEVICE_SERVICE)
    private readonly deviceServiceClient: ClientProxy,
  ) {
  }

  async transform(value: ClientAuthDto, {metatype}: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const client: ClientClass = new ClientClass(await lastValueFrom(
      this.clientServiceClient.send("get:client:uuid:identifier:by:login", {
        login: value.login
      }))
    )

    const verifiedDevice : ClientDevice = await lastValueFrom(
      this.deviceServiceClient.send('verify:device:activation:code', {
        userId: client.uuid,
        name: value.device.name,
        identifier: value.device.identifier,
        activationCode: value.device.activationCode,
        app: value.device.app
      })
    )

    console.log("verifiedDevice: ",verifiedDevice)

    if (!verifiedDevice) {
      throw new BadRequestException('Bad request')
    }

    console.log({
      criteria: {
        userId: client.uuid,
        app: verifiedDevice.app,
        name: verifiedDevice.name,
        identifier: verifiedDevice.identifier
      },
      partialValues: {
        pushToken: value.device.pushToken
      }
    })

    this.deviceServiceClient.emit('set:device:value', {
      criteria: {
        userId: client.uuid,
        app: verifiedDevice.app,
        name: verifiedDevice.name,
        identifier: verifiedDevice.identifier
      },
      partialValues: {
        pushToken: value.device.pushToken
      }
    })

    return client;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}

@Injectable()
export class WsErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        catchError(err =>
          throwError(() => {
            console.log("WsErrorsInterceptor: ", {...err});
            return new RpcException({...err});
          })
        )
      );
  }
}

@Controller()
@UseInterceptors(new WsErrorsInterceptor())
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {
  }

  @MessagePattern("generate:verification:code")
  @UsePipes(ClientDeviceAuthValidationPipe)
  async handleGenerateVerificationCode(@Payload() clientUnderVerification: ClientClass) {
    return this.authService.generateVerificationCode(clientUnderVerification)
  }

  @MessagePattern("auth:client:with:code")
  @UsePipes(ClientDeviceVerificationValidationPipe)
  async authClientWithCode(@Payload() verifiedClient: ClientClass) {
    return this.authService.generateJwtToken(verifiedClient)
  }
}