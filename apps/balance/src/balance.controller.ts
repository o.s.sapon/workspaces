import {CallHandler, Controller, ExecutionContext, Get, HttpException, Injectable, NestInterceptor, UseInterceptors} from "@nestjs/common";
import {BalanceService} from "./balance.service";
import {MessagePattern, RpcException} from "@nestjs/microservices";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable()
export class ErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        catchError(err =>
          throwError(() => {
            console.log("service err: ", err.status)
            return new RpcException({
              message: err.message,
              statusCode: err.statusCode || 500,
              status: 'error'
            });
          })
        )
      );
  }
}

@Controller()
export class BalanceController {
  constructor(private readonly balanceService: BalanceService) {
  }

  @UseInterceptors(ErrorsInterceptor)
  @MessagePattern({cmd: "balance"})
  getHello(): string {
    return this.balanceService.getHello();
  }
}
