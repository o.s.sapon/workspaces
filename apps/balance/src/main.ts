import { NestFactory } from '@nestjs/core';
import { BalanceModule } from './balance.module';
import {MicroserviceOptions, Transport} from "@nestjs/microservices";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(BalanceModule, {
    transport: Transport.TCP,
    options: {
      host: 'connect_balance',
      port: 4000
    }
  });
  await app.listen();
}
bootstrap();
