import {IsNotEmpty, IsString, Length} from "class-validator";

export class CreateDeviceInfoDto {
  @IsString()
  @IsNotEmpty()
  readonly identifier: string;

  @IsString()
  @IsNotEmpty()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  readonly app: string;
}

export class DeviceInfoDto extends CreateDeviceInfoDto {
  @IsString()
  @IsNotEmpty()
  @Length(4, 4)
  readonly activationCode: string;

  @IsString()
  @IsNotEmpty()
  readonly pushToken: string;
}