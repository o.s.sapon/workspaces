import {IsDefined, IsNotEmpty, IsNotEmptyObject, IsString, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
import {CreateDeviceInfoDto} from "@app/dto/create-device.dto";

export class VerificationCodeIntentDto {
  @IsString()
  @IsNotEmpty()
  readonly login: string;

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => CreateDeviceInfoDto)
  readonly device: CreateDeviceInfoDto

}