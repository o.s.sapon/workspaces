import { MigrationInterface, QueryRunner } from "typeorm";

export class addedDefaultToDeviceIsCallingColumn1657147105789 implements MigrationInterface {
    name = 'addedDefaultToDeviceIsCallingColumn1657147105789'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "devices" ALTER COLUMN "is_calling" SET DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "devices" ALTER COLUMN "is_calling" DROP DEFAULT`);
    }

}
