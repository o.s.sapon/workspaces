import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity, Generated,
  OneToMany,
  PrimaryGeneratedColumn,
  TableInheritance,
  Unique,
  UpdateDateColumn
} from "typeorm";
import { ChildEntity } from 'typeorm';
import {SocialAccountEntity} from "@app/database/entities/user/social-account.entity";
import {DeviceEntity} from "@app/database/entities/device/device.entity";

export abstract class UserEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    unique: true
  })
  @Generated("uuid")
  uuid: string

  @Column({
    unique: true
  })
  login: string

  @Column({
    nullable: true
  })
  password: string

  @Column({
    nullable: true
  })
  email: string

  @DeleteDateColumn({
    name: 'deleted_at'
  })
  deletedAt: Date

  @UpdateDateColumn({
    name: 'updated_at'
  })
  updatedAt: Date

  @CreateDateColumn({
    name: 'created_at'
  })
  createdAt: Date
}

@Entity('clients')
export class ClientEntity extends UserEntity {
  @Column({
    name: 'stripe_customer_id',
    nullable: true
  })
  public stripeCustomerId: string;

  @OneToMany(() => SocialAccountEntity, social => social.user)
  public socialAccounts: SocialAccountEntity[];

  @OneToMany(() => DeviceEntity, social => social.client)
  public devices: DeviceEntity[];
}