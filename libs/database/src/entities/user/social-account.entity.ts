import {Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {ClientEntity} from "@app/database";

@Entity({
  name: 'social_accounts'
})
export class SocialAccountEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    name: 'user_id',
    type: "uuid",
  })
  userId: string

  @Column()
  type: string

  @Column({
    name: 'social_id'
  })
  socialId: string

  @UpdateDateColumn({
    name: 'updated_at'
  })
  updatedAt: Date

  @CreateDateColumn({
    name: 'created_at'
  })
  createdAt: Date

  @ManyToOne(() => ClientEntity, client => client.socialAccounts)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'uuid'
  })
  user: ClientEntity
}