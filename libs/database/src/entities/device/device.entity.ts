import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  TableInheritance,
  Unique,
  UpdateDateColumn
} from "typeorm";
import {ClientEntity} from "@app/database";

@Entity({
  name: 'devices'
})
export class DeviceEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    name: 'user_id',
    type: "uuid"
  })
  userId: string

  @Column()
  active: boolean

  @Column()
  name: string

  @Column()
  identifier: string

  @Column()
  app: string

  @Column({
    nullable: true,
    name: 'push_token'
  })
  pushToken: string

  @Column({
    nullable: true,
    name: 'socket_id'
  })
  socketId: string

  @Column({
    nullable: true,
    name: 'activation_code'
  })
  activationCode: string

  @Column({
    name: 'is_calling',
    default: false
  })
  isCalling: boolean

  @Column({
    nullable: true,
    name: 'locale'
  })
  locale: string

  @UpdateDateColumn({
    name: 'last_seen'
  })
  lastSeen: Date

  @CreateDateColumn({
    name: 'created_at'
  })
  createdAt: Date

  @ManyToOne(() => ClientEntity, client => client.devices)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'uuid'
  })
  client: ClientEntity
}