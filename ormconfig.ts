import {DataSource} from "typeorm";

export const connectionSource = new DataSource({
  migrationsTableName: 'migrations',
  type: 'postgres',
  host: 'db_connect',
  port: 5432,
  username: 'webadmin',
  password: 'XIMpxm59556',
  database: 'app',
  logging: false,
  synchronize: false,
  name: 'default',
  entities: ['/home/node/app/libs/database/src/**/**.entity{.ts,.js}'],
  migrations: ['/home/node/app/libs/database/src/migrations/**/*{.ts,.js}'],
  subscribers: ['/home/node/app/libs/database/src/subscriber/**/*{.ts,.js}']
});